from fastapi_test_utils import assert_dict_equal, assert_list_equal


def test_assert_dict_equal():
    response_json = {
        'user': {
            'last_name': None,
            'first_name': None,
            'middle_name': None,
            'birth_date': None,
            'gender': None,
            'photo_id': None,
            'id': 1,
            'photo': None,
            'created_at': '2024-11-05T19:04:39.657033+00:00',
            'updated_at': '2024-11-05T19:04:39.657033+00:00',
            'email': 'test@sportnetix.com',
            'phone': None,
            'confirmed': False,
            'password_set': False,
        },
        'auth_token': {
            'id': 1,
            'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzcG9ydG5ldGl4LXNzbyIsInN1YiI6IjEiLCJleHAiOjE4MjQxNDU0NzkuNjQ0MTk4LCJzZXJ2aWNlIjoic3NvIn0.stVsWLeKBR-6cGdH7p8Vv0Fd7VcxHZQTjKtdU2aNjPs',
            'expires_at': '2027-10-21T19:04:39.644198+00:00',
        },
    }
    should_be = {'user': {'email': 'test@sportnetix.com', 'phone': None}, 'auth_token': {'id': 1}}
    assert_dict_equal(should_be, response_json)


def test_assert_list_equal():
    response_json = [
        {
            'user': {
                'last_name': None,
                'first_name': None,
                'middle_name': None,
                'birth_date': None,
                'gender': None,
                'photo_id': None,
                'id': 1,
                'photo': None,
                'created_at': '2024-11-05T19:04:39.657033+00:00',
                'updated_at': '2024-11-05T19:04:39.657033+00:00',
                'email': 'test@sportnetix.com',
                'phone': None,
                'confirmed': False,
                'password_set': False,
            },
        },
        {
            'user': {
                'last_name': None,
                'first_name': None,
                'middle_name': None,
                'birth_date': None,
                'gender': None,
                'photo_id': None,
                'id': 2,
                'photo': None,
                'created_at': '2024-11-05T19:04:39.657033+00:00',
                'updated_at': '2024-11-05T19:04:39.657033+00:00',
                'email': 'test2@sportnetix.com',
                'phone': None,
                'confirmed': False,
                'password_set': False,
            },
        },
    ]
    assert_list_equal(
        [
            {'user': {'id': 1, 'email': 'test@sportnetix.com'}},
            {'user': {'id': 2, 'email': 'test2@sportnetix.com'}},
        ],
        response_json,
    )
