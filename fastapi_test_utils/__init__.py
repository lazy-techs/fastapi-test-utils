from typing import Any
from unittest import TestCase

from sqlalchemy import Connection, text


def loaddata(
    connection: Connection,
    table: Any,
    data: list[dict[str, Any]],
    erase_all: bool = True,
) -> None:
    if erase_all:
        connection.execute(text(f'TRUNCATE "{str(table.__table__)}" RESTART IDENTITY CASCADE'))

    for item in data:
        connection.execute(table.__table__.insert().values(**item))

    if hasattr(table.__table__.c, 'id'):
        connection.execute(
            text(f'SELECT setval(\'{str(table.__table__)}_id_seq\', (SELECT max(id) FROM "{table.__table__.name}"))')
        )


def assert_dict_equal(should_be: dict[str, Any], expected: dict[str, Any]) -> None:
    TestCase().assertDictEqual(should_be, _get_only_keys(should_be, expected))


def assert_list_equal(should_be: list[dict[str, Any]], expected: list[dict[str, Any]]) -> None:
    TestCase().assertEqual(len(should_be), len(expected))
    expected = [
        _get_only_keys(should_be[idx], expected[idx])
        if isinstance(should_be[idx], dict) and isinstance(expected[idx], dict)
        else expected[idx]
        for idx in range(len(should_be))
    ]
    TestCase().assertListEqual(should_be, expected)


def _get_only_keys(should_be: dict[str, Any], expected: dict[str, Any]) -> dict[str, Any]:
    valid_keys = should_be.keys()
    return {
        key: _get_only_keys(should_be[key], value)
        if isinstance(should_be[key], dict) and isinstance(value, dict)
        else _get_list_value(should_be[key], value)
        if isinstance(should_be[key], list) and isinstance(value, list)
        else value
        for key, value in expected.items()
        if key in valid_keys
    }


def _get_list_value(should_be: list[Any], expected: list[Any]) -> list[Any]:
    return [
        _get_only_keys(should_be[idx], expected[idx])
        if isinstance(should_be[idx], dict) and isinstance(expected[idx], dict)
        else _get_list_value(should_be[idx], expected[idx])
        if isinstance(should_be[idx], list) and isinstance(expected[idx], list)
        else expected[idx]
        for idx in range(len(should_be))
    ]
